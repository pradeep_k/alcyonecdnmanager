#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# ui dependency setup
# sudo pip install python-tk
# sudo pip install python-imaging-tk

import os.path
import thread

import Tkinter

import ttk
from Tkinter import *
from tkFileDialog import askopenfilenames

from pytools.tools.Debug.Debug import Debug
from src.S3Helper import S3Helper
from src.STHelper import STHelper

class GUIAlcyone():

    master = Tk()
    master.wm_attributes('-topmost', 1)   # hack to adjust depth issue

    master.wm_title("Alcyone")
    master.protocol("WM_DELETE_WINDOW", master.quit)
    master.grid_columnconfigure(0, weight=1)

    currentwindow   = None
    listbox         = None

    # TODO change gui behaviour via GUIType option
    GUIType_ALL = "ALL"
    GUIType_OnlyProgess = "ONLY_PROGRESS"
    GUIType_OnlyProgress_Reports = "ONLY_PROGRESS_REPORT"
    GUIType_OnlyProgess_Fail_Reports = "ONLY_PROGRESS_FAIL_REPORT"

    GUIOptions = GUIType_ALL

    bucket_name = StringVar()
    root        = StringVar()
    truncate    = StringVar()

    purge       = StringVar()

    hwind_token     = None
    hwind_acnt_hash = None

    total_progress = IntVar()

    successfully_pushed_keys = []

    logs={"UPDATE":[],"FAILED":[],"ERROR":[]}

    def keyBinding(self, master):
        master.bind("<Delete>", self.onDelKeyDown)
        pass

    def onDelKeyDown(self,event):
        self.removeSelectedFiles()

    #user controllable functions
    def fillListbox(self, list):
        for item in list:

            if os.path.isdir(item):
                continue

            if item not in self.listbox.get(0, END):
                self.listbox.insert(END, item)

    def addMoreFiles(self):
        Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
        self.list_files_selected = askopenfilenames()  # show an "Open" dialog box and return the path to the selected file
        self.fillListbox(self.list_files_selected)

    #TODO clear selected
    # def clearSelectedFiles(self):
    #     if self.listbox is not None:
    #         print "clear"
    #         # self.listbox.curselection().clear
    #         # while len(self.listbox.curselection()) > 0:
    #         self.listbox.selection_clear(0, END)

    def removeSelectedFiles(self):
        if self.listbox is not None:
            while len(self.listbox.curselection()) > 0:
                self.listbox.delete(self.listbox.curselection()[0])


    def StartPush(self, master):

        self.master.wm_title("Alcyone: Pushing to S3")

        # callback
        def OnProgress(status, message, item, progress):
            if status == "ERROR":
                self.logs["ERROR"].append(item + ", " + message)

            elif status == "UPDATE":
                self.logs["UPDATE"].append(item + ", " + message)
                self.successfully_pushed_keys.append(item)

                if progress > -1:
                    self.total_progress.set(progress)

            else:
                if status == "SUCCESS":
                    print "Success, All files have been pushed"

                    if self.purge.get() == "YES":
                        self.StartPurge(self.successfully_pushed_keys)
                    elif self.purge.get() == "NO":
                        if self.GUIOptions == self.GUIType_ALL or self.GUIOptions == self.GUIType_OnlyProgress_Reports:
                            self.currentwindow.withdraw()
                            self.OpenSummary(self.master)
                        else:
                            self.master.quit()

                elif status == "FAILED":
                    print "Failed to push files"

                    if self.purge.get() == "YES":
                        self.StartPurge(self.successfully_pushed_keys)
                    elif self.purge.get() == "NO":
                        if self.GUIOptions == self.GUIType_ALL or self.GUIOptions == self.GUIType_OnlyProgess_Fail_Reports or self.GUIOptions == self.GUIType_OnlyProgress_Reports:
                            self.currentwindow.withdraw()
                            self.OpenSummary(self.master)
                        else:
                            self.master.quit()

        def DrawProgress(master):

            popup = Toplevel(master)
            popup.protocol("WM_DELETE_WINDOW", self.master.quit)
            popup.wm_attributes('-topmost', 1)  # hack to adjust depth issue

            self.currentwindow = popup

            Label(popup, text="Bucket: " + self.bucket_name.get()).grid(row=0, column=0, sticky=W)
            Label(popup, text="Target: " + self.root.get()).grid(row=1, column=0, sticky=W)

            progressbar = ttk.Progressbar(popup, orient=HORIZONTAL, length=250, mode='determinate',
                                          variable=self.total_progress)
            progressbar.grid(row=3, column=0)

            self.centerWindow(popup)

            self.master.withdraw()

        if self.bucket_name is None or self.bucket_name == "":
            Debug.ShowErrorBox("Error", "Bucket name is none")
            return

        bucket = S3Helper.GetBucket(self.bucket_name.get())
        if bucket == "FAILED":
            Debug.ShowErrorBox("Error", "Cannot reach bucket")
            return

        elif bucket is None:
            Debug.ShowErrorBox("Error", "Unable to find bucket")
            return

        paths = self.listbox.get(0, END)
        if len(paths) < 1:
            print "nothing to push"
            return

        def start_pushing_to_s3():
            S3Helper.SendFilesToBucket(filepaths=paths, truncatepathkeyto=self.truncate.get(), bucket=bucket, rootkey=self.root.get(), progresscallback=OnProgress)

        DrawProgress(master)
        thread.start_new_thread(start_pushing_to_s3,())

    def StartPurge(self, purgepaths):

        self.master.wm_title("Alcyone: CDN Purge")

        def OnProgress(status, message, item=None, progress=-1):

            if progress != -1:
                t_progress.set(progress)

            if status == "SUCCESS":
                self.logs["UPDATE"].append(message)

                if self.GUIOptions == self.GUIType_ALL or self.GUIOptions == self.GUIType_OnlyProgress_Reports:
                    self.currentwindow.withdraw()
                    self.OpenSummary(self.master)
                else:
                    self.master.quit()

            elif status == "ERROR" or status == "FAILED":
                self.logs[status].append(message)

                if self.GUIOptions == self.GUIType_ALL or self.GUIOptions == self.GUIType_OnlyProgess_Fail_Reports or self.GUIOptions == self.GUIType_OnlyProgress_Reports:

                    self.currentwindow.withdraw()
                    self.OpenSummary(self.master)
                else:
                    self.master.quit()

        def DrawProgress(master, total_progress):

            popup = Toplevel(master)
            popup.protocol("WM_DELETE_WINDOW", self.master.quit)
            master.wm_attributes('-topmost', 1)
            popup.grid_columnconfigure(0,weight=1)

            self.currentwindow = popup

            Label(popup, text="Please wait, purge in progess").grid(row=0, column=0, sticky=W+E)

            progressbar = ttk.Progressbar(popup, orient=HORIZONTAL, length=250, mode='determinate', variable=total_progress)
            progressbar.grid(row=1, column=0, sticky=W+E)

            self.centerWindow(popup)

            self.master.withdraw()

        if self.hwind_token is None or self.hwind_acnt_hash is None:
            OnProgress("ERROR", "HIGH WIND Credentials missing, purge cancelled")
            return

        t_progress = IntVar()

        self.currentwindow.withdraw()
        DrawProgress(self.master,t_progress)

        paths = []
        for p in purgepaths:
            paths.append("http://" + self.bucket_name.get() + "/" + p)

        STHelper.PurgePaths(paths, self.hwind_token, self.hwind_acnt_hash, progresscallback=OnProgress,recursive=False)

    def OpenFileList(self, master, filepaths):

        self.master.wm_title("Files to push")

        # draw code
        def DrawCurrentPushSetting(master):
            frame = Frame(master=master, borderwidth=2)
            frame.pack(side=TOP, fill=BOTH, expand=True)
            frame.grid_columnconfigure(1, weight=1)

            Label(frame, text="Bucket").grid(row=0, sticky=N + W)
            Label(frame, text="Target").grid(row=1, sticky=N + W)

            Entry(frame, text=self.bucket_name.get(), textvariable=self.bucket_name, bg='white').grid(row=0, column=1,
                                                                                                      sticky=W + E)
            Entry(frame, text=self.root.get(), textvariable=self.root, bg='white').grid(row=1, column=1, sticky=W + E)

            checkbox_purge = Checkbutton(frame, text="Purge", variable=self.purge, onvalue="YES", offvalue="NO")
            checkbox_purge.grid(row=0, column=3, sticky=S + E)

            # TODO setting panel to config hwind credentials
            # self.settings_image = ImageTk.PhotoImage(file='res/settings.bmp')
            # Button(frame, text="settings", image=self.settings_image).grid(row=0, column=3, sticky=N+E)

            push_button = Button(frame, text="Push", command=lambda: self.StartPush(master))
            push_button.grid(row=1, column=3, sticky=S + E)
            # push_button.pack(side=RIGHT)

        def DrawAction(master):
            frame = Frame(master=master, relief=FLAT, borderwidth=2)
            frame.pack(fill=BOTH, expand=True)
            frame.grid_columnconfigure(3, weight=1)

            Button(frame, text="Add More...", command=self.addMoreFiles).grid(row=0, column=0, sticky=W)
            Button(frame, text="Remove", command=self.removeSelectedFiles).grid(row=0, column=1, sticky=W)

            # clear_selection_button = Button(frame, text="Clear All", command=self.clearSelectedFiles())

            Label(frame, text=" Truncate").grid(row=0,column=2, sticky=E)
            Entry(frame, text=self.truncate.get(), textvariable=self.truncate, bg='white').grid(row=0, column=3, sticky=W+E)

        def DrawList(master, filepaths):

            frame = Frame(master=master, borderwidth=2)

            scrollbar = Scrollbar(frame, orient="vertical")

            self.listbox = Listbox(frame, width=50, height=20, selectmode='multiple', yscrollcommand=scrollbar.set,
                                   background="white")
            self.listbox.pack(side="left", fill="both", expand=True)

            scrollbar.pack(side="right", fill="y")
            scrollbar.config(command=self.listbox.yview)

            if filepaths is not None and len(filepaths) > 0:
                self.fillListbox(filepaths)

            frame.pack(fill=BOTH, expand=True)

        DrawList(master,filepaths=filepaths)
        DrawAction(master)
        DrawCurrentPushSetting(master)

        self.centerWindow(master)

        return

    def OpenSummary(self, master):

        popup = Toplevel(master)
        popup.protocol("WM_DELETE_WINDOW", self.master.quit)
        popup.wm_attributes('-topmost', 1)  # hack to adjust depth issue
        self.currentwindow = popup
        popup.wm_title("Alcyone: Summary ")


        frame = Frame(master=popup, borderwidth=2)

        # Label(frame, text="Report", anchor=E, justify=LEFT, height=1)

        scrollbar = Scrollbar(frame, orient="vertical")

        listbox = Listbox(frame, width=50, height=20, selectmode='multiple', yscrollcommand=scrollbar.set,
                          background="white")
        listbox.pack(side="left", fill="both", expand=True)

        scrollbar.pack(side="right", fill="y")
        scrollbar.config(command=self.listbox.yview)

        frame.pack(fill=BOTH, expand=True)
        # print "------FAILED-----"
        for f in self.logs["FAILED"]:
            listbox.insert(END, f)
            listbox.itemconfig(END, {'bg': '#ee2c2c', 'fg': '#8b1a1a', 'font':'Helvetica 6 bold'})

        # print "------ERROR-----"
        for e in self.logs["ERROR"]:
            listbox.insert(END, e)
            listbox.itemconfig(END, {'bg': '#eec900'})

        # print "------UPDATES-----"
        for s in self.logs["UPDATE"]:
            listbox.insert(END, s)
            listbox.itemconfig(END, {'bg': '#d6f5d6'})

        self.centerWindow(popup)

    def centerWindow(self, toplevel):
        toplevel.update_idletasks()
        w = toplevel.winfo_screenwidth()
        h = toplevel.winfo_screenheight()
        size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
        x = w / 2 - size[0] / 2
        y = h / 2 - size[1] / 2
        toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))

    def __init__(self, filepaths=None, bucketname=None, root=None, truncate=None, purge=False, hwndtoken=None, hwndacnthash=None, guitype=GUIType_ALL):

        if bucketname is not None:
            self.bucket_name.set(bucketname)

        if root is not None:
            self.root.set(root)

        if truncate is not None:
            self.truncate.set(truncate)

        purge_var = "YES" if purge else "NO"
        self.purge.set(purge_var)

        if self.purge.get() == "YES":

            if hwndtoken is not None and hwndacnthash is not None:
                self.hwind_token        = hwndtoken
                self.hwind_acnt_hash    = hwndacnthash
            else:
                print "GUI Alcyon hwind credentials None"

        self.GUIOptions = guitype
        self.keyBinding(self.master)

        self.OpenFileList(self.master,filepaths=filepaths)

        if self.GUIOptions == self.GUIType_OnlyProgess or self.GUIOptions == self.GUIType_OnlyProgess_Fail_Reports or self.GUIOptions == self.GUIType_OnlyProgress_Reports:
            self.StartPush(self.master)

        mainloop()

if __name__ == "__main__":
    win = GUIAlcyone()