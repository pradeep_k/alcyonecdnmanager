#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import os.path
import sys

from guialcyone import GUIAlcyone

from pytools.tools.IO import FileHelper
from src.S3Helper import S3Helper
from src.STHelper import STHelper


# test_bucket = "test.cosiproductions.com"
# test_dir    = "/Users/pradeepk/Documents/test_folder"

# test_HWIND_TOKEN         = "54d6ebfa98d414f77a473e5ec5f46fd61cb233bdc26f286640015c724e0989c0"
# test_HWIND_PATH          = "http://dev.cosiproductions.com/Ali/iOS/1.0.5"
# test_HWIND_ACCOUNTHASH   = "k4q4v8n4"

def main():
    parser = argparse.ArgumentParser(prog="Alcyone", description='prints arguments')

    # main commands
    parser.add_argument("send", nargs='+', help="send files and directories")
    parser.add_argument("-gui", nargs=1, help="show gui")

    parser.add_argument("-b", nargs=1, help="bucket name")
    parser.add_argument("-root", nargs=1, help="sets bucket root")
    parser.add_argument("-ignore", nargs="+", help="ignore file with extension")
    parser.add_argument("-truncate", nargs=1, help="strips path when uploading to bucket")

    parser.add_argument("-purgepaths", nargs="?", help="Purges highwind paths after upload, Usage: \n -purgepaths <paths>")
    parser.add_argument("-hwind", nargs=2,
                        help="highwind credential *required if purge is called, Usage: -hwind <token> <account_hash>")

    args = parser.parse_args()

    # bucket check
    bucket_name = args.b[0]
    if bucket_name is None:
        print "bucket name cannot be none"
        return

    bucket = S3Helper.GetBucket(bucket_name)

    if bucket == "FAILED":
        print "cannot reach bucket"
        sys.exit(1)
    elif bucket is None:
        print "unable to find bucket"
        sys.exit(1)

    bucket_root = args.root[0]

    # if args.ignore != None:
    #     ignore_filetypes = args.ignore

    truncate = None
    if args.truncate is not None:
        truncate = args.truncate[0]

    # Purge
    purge_list = None
    if args.purgepaths is not None:
        purge_list = args.purgepaths

    # HWIND
    purge_credentials = None
    if args.hwind is None:
        print "hwind credentials missing, will ignore purge"
    else:
        purge_credentials = {'token': args.hwind[0], 'achash': args.hwind[1]}

    #gui
    gui = None
    if args.gui is not None:
        if args.gui[0] == GUIAlcyone.GUIType_ALL or args.gui[0] == GUIAlcyone.GUIType_OnlyProgess or args.gui[0] == GUIAlcyone.GUIType_OnlyProgress_Reports or args.gui[0] == GUIAlcyone.GUIType_OnlyProgess_Fail_Reports:
            gui = args.gui[0]
        else:
            "unknown gui option"

    if not args.send is None:

        if gui is not None:

            hwind_token = None
            hwind_hash  = None

            purge_bool = purge_credentials is not None
            if purge_credentials is None:
                print "skipping purge, credentials missing"
            else:
                hwind_token= purge_credentials['token']
                hwind_hash = purge_credentials['achash']

            GUIAlcyone(args.send[1:], bucketname=bucket_name, root=bucket_root, truncate=truncate, purge=purge_bool, hwndtoken=hwind_token, hwndacnthash=hwind_hash, guitype=gui)

        #TODO update cmdline calls
        # else:
        #     for i in range(1, len(args.send)):
        #
        #         srcpath = args.send[i]
        #
        #         if not os.path.exists(srcpath):
        #             print ("path does not exist, skipping: ", srcpath)
        #         else:
        #             if os.path.isdir(srcpath):
        #                 S3Helper.SendDirectoryToBucket(srcpath, truncate, bucket, ignore_filetypes, rootkey=bucket_root)
        #             else:
        #                 if FileHelper.FileHelper.DoesFileHaveTheseExtensions(srcpath, extensionlist=ignore_filetypes):
        #                     print "File ignored, ", srcpath
        #                     continue
        #
        #                 if truncate is None:
        #                     truncate = os.path.split(srcpath)[0]
        #
        #                 S3Helper.SendToBucket(filepath=srcpath, truncatepathkeyto=truncate, bucket=bucket,
        #                                       rootkey=bucket_root)

            # if not purge_list is None:
            # if True:
            #
            #     if purge_credentials is None:
            #         print "skipping purge, credentials missing"
            #         sys.exit(0)
            #
            #     purgepaths=[]
            #     for p in purge_list:
            #         purgepaths.append("http://" + bucket.name + "/" + p)
            #
            #     STHelper.PurgePaths(paths=purgepaths, token=purge_credentials['token'], account_hash=purge_credentials['achash'])


if __name__ == "__main__":
    main()
