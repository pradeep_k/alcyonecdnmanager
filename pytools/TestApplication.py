from tools.Actions.UniqueAction import UniqueAction
# from tools.base.ToolDecorations import acceptsMemberOfClass
from tools.base.ToolObject  import ToolObject


class Test(ToolObject):

    v = "12"

    def SayTick(self, *arg):
        print "tick!"

    def _OnDestroy_(self):
        print "destroyed"


def Run():


    t = Test()

    # acceptsMemberOfClass(t, ToolObject)

    action = UniqueAction()
    action.Subscribe(t.SayTick)
    t.DestroySelf()

    # action.UnSubscribe(sayTick)
    # action.UnSubscribe(sayTick)

    action.Invoke()

Run()

