import errno
import os

class FileHelper:

    @staticmethod
    def GetAllFilesUnderDirectory(directory=None):
        onlyfiles = []

        if directory == None:
            return onlyfiles

        for (_dir, _, files) in os.walk(directory):
            for f in files:
                path = os.path.join(_dir, f)
                if os.path.exists(path):
                    # print path #debug only
                    onlyfiles.append(path)

        return onlyfiles

    @staticmethod
    def GetFile(path, mode="r"):
        # Get a file if available, generates a new one if not

        try:
            os.makedirs(os.path.dirname(path))  # tries to make dir, if dir does not already exist
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        try:
            file = open(path, mode)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        return file

    @staticmethod
    def IsFileEmptyFromPath(filepath):
        return (os.stat(filepath).st_size == 0)

    @staticmethod
    def IsFileEmpty(file):
        return (FileHelper.GetFileSize(file) == 0)

    @staticmethod
    def RemoveFilesWithExtensions(filepaths, extensionlist):
        files = []
        for f in filepaths:
            if not FileHelper.DoesFileHaveTheseExtensions(f, extensionlist=extensionlist):
                files.append(f)

        return files

    @staticmethod
    def DoesFileHaveTheseExtensions(filepath, extensionlist):
        split_paths = os.path.splitext(filepath)

        if len(split_paths) < 1:
            return False

        file_extension = split_paths[1]

        #remove period
        if len(file_extension) > 0 and file_extension[0] == '.':
            file_extension = file_extension[1:]

        return file_extension in extensionlist

    @staticmethod
    def GetFileSize(file):

        pointer_original_position = file.tell() #store the pointer location, to reset it to the original position nicely

        file.seek(0, os.SEEK_END)
        size = file.tell()

        file.seek(pointer_original_position, os.SEEK_SET)

        return size

