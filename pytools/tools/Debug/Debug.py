import Tkinter
import tkMessageBox

import sys


class Debug:
    Error_Key   = "ERROR"
    Success_Key = "SUCCESS"
    Failed_Key  = "FAILED"
    Message_Key = "MESSAGE"
    Update_Key  = "UPDATE"

    @staticmethod
    def ShowErrorBox(title, message):
        tkMessageBox.showerror(title=title, message=message)

    @staticmethod
    def LogAndCallProgress(status="", message="",item=None, callback=None, progress=-1):
        sys.stdout.write(message)
        # print message

        if callback is not None:
            callback(status, message, item,progress)