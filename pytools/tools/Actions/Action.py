from ...tools.base.ToolHelper       import get_class_that_defined_method
from ...tools.base.ToolDecorations  import accepts
from ...tools.base.ToolObject       import ToolObject

class Action(ToolObject):
    _protected_func_ref_list_ = [ToolObject()] #dummy declaration

    @accepts(ToolObject, ToolObject)
    def __cleanup_ghosts__(self, toolobject):
        clean_list = []
        for f in toolobject:
            if type(f) is ToolObject:
                if not f.IsGhost:
                    clean_list.append(f)

        return clean_list

    def Subscribe(self, function):
        assert isinstance(get_class_that_defined_method(function), ToolObject), \
            "arg: %r is not from a class type, %s" % (get_class_that_defined_method(function), ToolObject)

        self._protected_func_ref_list_.append(function)

    def UnSubscribe(self,function):
        assert isinstance(get_class_that_defined_method(function), ToolObject), \
            "arg: %r is not from a class type, %s" % (get_class_that_defined_method(function), ToolObject)

        if self._protected_func_ref_list_.__contains__(function): #try to remove, only if there is reference
            self._protected_func_ref_list_.remove(function)

    def Invoke(self):
        self.Invoke(None)

    def Invoke(self, *arg):
        #remove ghost references
        self._protected_func_ref_list_  = self.__cleanup_ghosts__(self._protected_func_ref_list_)

        for f in self._protected_func_ref_list_:
            f(arg)

