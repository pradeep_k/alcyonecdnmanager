from Action import Action


class UniqueAction(Action):
    def Subscribe(self, function):
        if not self._protected_func_ref_list_.__contains__(function):
            self._protected_func_ref_list_.append(function)

