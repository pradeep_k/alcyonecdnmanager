# from tools.base.ToolHelper import get_class_that_defined_method


def accepts(*types):
    def check_accepts(f):
        assert len(types) == f.func_code.co_argcount

        def new_f(*args, **kwds):
            for (a, t) in zip(args, types):
                assert isinstance(a, t), \
                    "arg %r does not match %s" % (a, t)
            return f(*args, **kwds)

        new_f.func_name = f.func_name
        return new_f

    return check_accepts

# def acceptsMemberOfClass(memb, classtype):
#     assert isinstance(get_class_that_defined_method(memb), classtype), \
#         "arg: %r is not from a class type, %s" % (get_class_that_defined_method(memb), classtype)
