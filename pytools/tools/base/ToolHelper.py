import inspect


def get_class_that_defined_method(meth):
    for cls in inspect.getmro(meth.im_class):
        if meth.__name__ in cls.__dict__:
            return cls
    return None

# def get_class_that_defined_variable(variable):
#     for cls in inspect.getmembers(variable.__).:
#         if variable.__name__ in cls.__dict__:
#             return cls
#     return None
#
# def get_class_that_defined_member(member):
#     r_value = None
#
#     if(inspect.ismethod(member)):
#         r_value = get_class_that_defined_method(member)
#     else:
#         r_value = get_class_that_defined_variable(member)
#
#     return r_value