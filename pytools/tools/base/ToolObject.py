

class ToolObject(object):
    __is_ghost__ = False # has the object been destroyed

    def _OnDestroy_(self):
        pass

    #publics
    @property
    def IsGhost(self):
        return self.__is_ghost__

    def DestroySelf(self):
        self._OnDestroy_()

        if self.__is_ghost__:
            del self
            return

        self.__is_ghost__ = True

