from distutils.core import setup

setup(
    name='alcyonecdnmanager',
    version='0.01',
    packages=['', 'src', 'pytools', 'pytools.tools', 'pytools.tools.IO', 'pytools.tools.base', 'pytools.tools.Actions'],
    install_requires=['boto3','striketracker'],
    url='',
    license='MIT',
    author='pradeep kumar rajamanickam',
    author_email='raj.pradeep.kumar@gmail.com',
    description='command line tool to upload and manage amazon s3 & highwind'
)
