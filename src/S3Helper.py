import os
import boto3
import botocore
import sys

from pytools.tools.Debug.Debug import Debug
from src.Base import Base
from pytools.tools.IO.FileHelper import FileHelper

class S3Helper(Base):

    #filenames
    default_filename_credentials = "credentials"
    default_filename_config      = "config"

    #paths
    # default_home = os.environ["HOME"]
    default_home = os.path.expanduser('~') #for platform independent user directory
    default_aws_path = os.path.join(default_home, ".aws")

    try:
        __s3__ = boto3.resource('s3')
    except botocore.exceptions.ClientError as e:
        Debug.LogAndCallProgress("ERROR", "Boto error" +str(e))
        Debug.ShowErrorBox("Error", "Boto error" +str(e))
        raise


    @staticmethod
    def CreateBucket(name=None):
        if name is None:
            return None

        print "Trying to create new bucket, ", name

        existing_bucket = S3Helper.GetBucket(name=name)
        if not existing_bucket == None:
            print "Aborted bucket creation, ", name, " already exists"
            return existing_bucket

        newbucket = S3Helper.__s3__.create_bucket(Bucket=name)

        print name, ", bucket created successfully"

        return newbucket

    @staticmethod
    def SendDirectoryToBucket(directorypath=None, truncatepathkeyto=None, bucket=None, ignoretype=None, rootkey=None, progresscallback=None):
        if directorypath == None:
            Debug.LogAndCallProgress("ERROR", "Dir path none", item=directorypath, callback=progresscallback)
            return

        if truncatepathkeyto == None:
            truncatepathkeyto=directorypath

        filesindirectory = FileHelper.GetAllFilesUnderDirectory(directorypath)

        S3Helper.SendFilesToBucket(filepaths=filesindirectory, truncatepathkeyto=truncatepathkeyto, bucket=bucket, ignoretype=ignoretype, rootkey=rootkey, progresscallback=progresscallback)

    @staticmethod
    def SendFilesToBucket(filepaths=None, truncatepathkeyto=None, bucket=None, ignoretype=None, rootkey=None, progresscallback=None):

        if filepaths == None:
            Debug.LogAndCallProgress("ERROR", "file paths none", callback=progresscallback)

        # filter out ignored file types
        if not ignoretype is None:
            files = FileHelper.RemoveFilesWithExtensions(filepaths, ignoretype)
            Debug.LogAndCallProgress("MESSAGE","Ignored file count: " +str(len(filepaths) - len(files)), callback=progresscallback)

            filepaths = files


        total_item_count = len(filepaths)
        Debug.LogAndCallProgress("MESSAGE", "Total files to send: " +str(total_item_count), callback=progresscallback)

        sentcount   = {'value': 0}
        failcount   = {'value': 0}

        def getCurrentItemsProcessedCount():
            return sentcount['value'] + failcount['value']

        def getCurrentProgress():
            #debug only
            #print  str(getCurrentItemsProcessedCount()) +"/" +str(total_item_count) +"=" +str((float(getCurrentItemsProcessedCount()) / float(total_item_count)))
            return float(getCurrentItemsProcessedCount()) / float(total_item_count) * 100.0

        def OnFileSent(message, item):
            sentcount['value'] += 1
            Debug.LogAndCallProgress("UPDATE", "item sent: " + str(getCurrentItemsProcessedCount()) + "/" + str(total_item_count), item=item, callback=progresscallback, progress=getCurrentProgress())
            pass

        def OnSendFailed(message, item):
            failcount['value'] += 1
            Debug.LogAndCallProgress("ERROR", "failed to send item:" + str(getCurrentItemsProcessedCount()) + "/" + str(total_item_count), item=item, callback=progresscallback, progress=getCurrentProgress())
            pass

        for element in filepaths:
            def onprogress(status, message, item, progress):
                if status == "SUCCESS":
                    OnFileSent(message, item)
                elif status == "FAILED" or status == "ERROR":
                    OnSendFailed(message, item)

            S3Helper.SendToBucket(filepath=element, truncatepathkeyto=truncatepathkeyto, bucket=bucket, rootkey=rootkey, progresscallback=onprogress)


        if failcount['value'] > 0:
            Debug.LogAndCallProgress("FAILED", str(failcount['value']) +" items failed to send", callback=progresscallback, progress=getCurrentProgress())
        else:
            Debug.LogAndCallProgress("SUCCESS", "All items have been sent successfully", callback=progresscallback, progress=getCurrentProgress())


    @staticmethod
    def SendToBucket(filepath=None, truncatepathkeyto=None, bucket=None, rootkey=None, progresscallback=None):

        if bucket == None:
            Debug.LogAndCallProgress("ERROR", "bucket none", callback=progresscallback)
            return

        if filepath == None:
            Debug.LogAndCallProgress("ERROR", "file path none", callback=progresscallback)
            return

        try:
            data = open(filepath, 'rb')
        except OSError:
            Debug.LogAndCallProgress("ERROR", "OSERROR: " +OSError.message, callback=progresscallback)
            return "ERROR"

        #figure out relative path if truncatepathtill is given
        key = filepath

        if not truncatepathkeyto == None:
            key = os.path.relpath(filepath, truncatepathkeyto)


        if not rootkey == None:

            #add slash if not present in the end
            if not rootkey.endswith("/"):
                rootkey = rootkey + "/"

            # remove slash if one is present in front (otherwise an empty dir will be created infront)
            if rootkey.startswith("/"):
                rootkey = rootkey[1:]

            if key.startswith("/"):
                key = key[1:]

            key = rootkey + key

        print key

        s3obj = None
        try:
            s3obj = bucket.put_object(Key=key, Body=data)
        except botocore.exceptions.ClientError as e:
            Debug.LogAndCallProgress("ERROR", "Boto Error" +str(e), key,callback=progresscallback)

        if not s3obj is None:
            Debug.LogAndCallProgress("SUCCESS",'Sent...' +str(s3obj), key, callback=progresscallback)
            return "SUCCESS"

        Debug.LogAndCallProgress("FAILED", "Failed To Send..." + str(key), key,callback=progresscallback)
        return "FAILED"

        #end of function
    @staticmethod
    def GetFileFromBucket(key,bucketname,targetfile,progresscallback=None):
        s3_client = boto3.client('s3')
        try:
            s3_client.download_file(bucketname, key, targetfile)
        except botocore.exceptions.ClientError as e:
            Debug.LogAndCallProgress("ERROR", "Boto Error" + str(e), key, callback=progresscallback)
            # Debug.ShowErrorBox("Error", str(e))
            raise

    @staticmethod
    def GetFromBucket(directory=None, bucket=None):
        return S3Helper.Stub()

    @staticmethod
    def GetBuckets():
        buckets = None
        try:
            buckets = S3Helper.__s3__.buckets.all()
        except botocore.exceptions.EndpointConnectionError as e:
            Debug.LogAndCallProgress("ERROR","Unable to reach boto buckets")
            Debug.ShowErrorBox("Error", str(e))

            return "FAILED"

        return buckets

    @staticmethod
    def GetBucket(name=None):
        # type: (basestring) -> Bucket

        buckets = S3Helper.GetBuckets()

        if buckets == "FAILED":
            return buckets

        if name is None or buckets is None:
            return None

        try:
            for b in buckets:
                if b.name == name:
                    return b
        except botocore.exceptions.EndpointConnectionError as e:
            Debug.ShowErrorBox("Error", str(e))

        return None


    @staticmethod
    def SetCredentials(access_key="DEFAULT_KEY", secret_access_key="DEFAULT_SECRET", region="DEFAULT_REGION"):
        credentials = None

        if S3Helper.GetCredentialFile() == None:
            print "No credential files found, trying to create a new one"
            credentials = S3Helper.GenerateNewCredentialFile(access_key=access_key,access_secret=secret_access_key)
            if credentials == None:
                print "Failed to create new file, at " +S3Helper.GetAWSFilePathForCredentials()
                return "Failed"

        return "Success" #check writable permission

    #Setters
    @staticmethod
    def SetAccessKey(key="DEFAULT_KEY"):
        S3Helper.SetCredentials(access_key=key)

    @staticmethod
    def SetSecretAccessKey(key="DEFAULT_SECRET"):
        S3Helper.SetCredentials(secret_access_key=key)

    @staticmethod
    def SetRegion(location="DEFAULT_REGION"):
        S3Helper.SetCredentials(region=location)

    #Getters
    @staticmethod
    def GetAccessKey():
        return S3Helper.Stub()

    @staticmethod
    def GetSecretAccessKey():
        return S3Helper.Stub()

    @staticmethod
    def GetRegion():
        return S3Helper.Stub()

    @staticmethod
    def GetCredentialFile():
        try:
            file = FileHelper.GetFile(S3Helper.GetAWSFilePathForCredentials(), "a")

            if(FileHelper.IsFileEmpty(file)):
                # generate a content for the file
                access_key = "DEFAULT_KEY"
                access_secret = "DEFAULT_SECRET"

                content = "[default]\n" \
                          "aws_access_key_id = " + access_key + "\n" \
                           "aws_secret_access_key = " + access_secret

                file.write(content)

            file.close()
        except OSError:
            raise

        return file

    @staticmethod
    def GetConfigFile():
        try:
            file = FileHelper.GetFile(S3Helper.GetAWSFilePathForConfig(), "a")

            if (FileHelper.IsFileEmpty(file)):
                # generate a content for the file
                region = "us-east-1"

                content = "[default]\n" \
                          "region = " + region

                file.write(content)

            file.close()
        except OSError:
            raise

        return file

    ## File Path Helpers
    @staticmethod
    def GetAWSFilePathForConfig():
        return S3Helper.__getAWSFilePath(S3Helper.default_filename_config)

    @staticmethod
    def GetAWSFilePathForCredentials():
        return S3Helper.__getAWSFilePath(S3Helper.default_filename_credentials)

    @staticmethod
    def __getAWSFilePath(filename):
        # type: string
        return os.path.join(S3Helper.default_aws_path, filename)



