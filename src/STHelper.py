
from pytools.tools.Debug.Debug import Debug

import striketracker
from striketracker import APIClient
import sys
import time

class STHelper():
    @staticmethod
    def PurgePaths(paths=None, token=None, account_hash=None, progresscallback=None, recursive=True):

        # Initialize client
        client = APIClient(token=token)

        # Send purge request
        Debug.LogAndCallProgress('MESSAGE', 'Sending purge')

        urls = []
        for url in paths:
            url = url.strip() # remove empty spaces
            Debug.LogAndCallProgress('MESSAGE', 'To purge url: ' +url)
            urls.append({
                "url": url,
                "recursive": recursive
            })

        try:
            job_id = client.purge(account_hash, urls)
        except TypeError as e:
            # print e.message
            Debug.LogAndCallProgress("FAILED", "Purge failed: " +str(e))
            Debug.ShowErrorBox("Purge", str(e))
            return "FAILED"
        except striketracker.APIError as e:
            Debug.LogAndCallProgress("FAILED", "Purge failed: " +str(e))
            Debug.ShowErrorBox("Purge", str(e))
            return "FAILED"


        # Poll for purge completion
        progress = 0.0
        while progress < 100.0:
            try:
                progress = client.purge_status(account_hash, job_id) * 100
            except striketracker.APIError as e:
                Debug.LogAndCallProgress("ERROR", "PURGE: " +str(e))
                return "ERROR"

            sys.stdout.write('\r')
            Debug.LogAndCallProgress('UPDATE','Progress...' +str(int(progress))+ "%", callback=progresscallback, progress=progress)
            sys.stdout.flush()
            time.sleep(0.5)

        Debug.LogAndCallProgress('SUCCESS', '\nPurge Complete\n', callback=progresscallback)
        return "SUCCESS"